import React from 'react';
import {BrowserRouter as Router,Switch,Route,Link} from 'react-router-dom';
import './index.css';
import LoginComponent from './Login/login.component';
import QuestionsComponent from './Question/question.component';
import NewQuestionComponent from './NewQuestion/NewQuestion.component';
import GroupsComponent from './Groups/groups.component';
import PagesComponent from './Pages/pages.component';
import NewPageComponent from './Pages/newpage.component';
import {connect} from 'react-redux';
import Modal from 'react-modal';


class App extends React.Component{
    constructor()
    {
        super();
    }
    render(){
        return(
            <Router>
            <div>
            <div class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
            <div class="container">
             <a href="" class="navbar-brand">Admin Tool</a>
             <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
             </button>
             {this.props.isLoggedIn ?
             <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav">
                 <li class="nav-item">                   
                   <Link to={'/question'}>
                   <a class="nav-link">Manage Questions</a>
                   </Link>           
                 </li>
                  {/* <li class="nav-item">
                   <Link to={'/add'}>
                   <a class="nav-link">Add New Question</a>
                   </Link> 
                 </li>  */}
                 <li class="nav-item">
                   <Link to={'/groups'}>
                   <a class="nav-link">Manage Groups</a>
                   </Link> 
                 </li> 
                 <li class="nav-item">
                   <Link to={'/pages'}>
                   <a class="nav-link">Manage Pages</a>
                   </Link> 
                 </li> 
                 {/* <li class="nav-item">
                   <Link to={'/newpage'}>
                   <a class="nav-link">Add New Page</a>
                   </Link> 
                 </li> */}
                   {/* <li className="nav-item">
                       <Link to={'/newgrouppage'}>
                           <a className="nav-link">Add Page To Group</a>
                       </Link>
                   </li> */}
               </ul>
         
               <ul class="nav navbar-nav ml-auto">        
                 <li class="nav-item">
                   <a class="nav-link" href="" target="_blank">Sign Out</a>
                 </li>
               </ul>
             </div>:""
             }
           </div>
         </div>
         <div class="container">    
             <h1>Question</h1>
             <br/>
              <Route exact path="/" component={LoginComponent}/>
              <Route exact path="/add" component= {NewQuestionComponent}/>
              <Route  path="/question" component={QuestionsComponent}/>
              <Route  path="/groups" component={GroupsComponent}/>
              <Route path ="/pages" component={PagesComponent}/>
              {/* <Route path ="/newpage" component={NewPageComponent}/>
             <Route path ="/newgrouppage" component={NewGroupComponent}/> */}
         </div>
         </div>
         </Router>
        );
    }
}

var mapStateToProps = function(state){
  console.log("inside mapStatetoProps",state);
  return {
    isLoggedIn:state.isLoggedIn
    
  };
}

export default connect(mapStateToProps,null)(App);