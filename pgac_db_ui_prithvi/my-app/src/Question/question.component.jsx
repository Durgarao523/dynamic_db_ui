import React from 'react';
import { navigation } from 'react-router';


class QuestionsComponent extends React.Component {

    handleSubmit(event) {
      event.preventDefault();
    
    };

   
    render(){
        return(
            <div>
        <div class="card border-primary mb-3">
        <div class="card-header">Question List</div>
        <div class="card-body">
        <div class="container">
          <div class="row">
            <div class="col-sm-7">
              <div class="form-group">
                <label for="exampleInputCategory" class="text-info">Page Uid</label>
                <select class="custom-select">
                  <option selected="">Select page</option>                  
                  <option>General Information</option>                  
                  <option>Policy Holder</option>                  
                  <option>Driver</option> 
                  <option>Vehicle Information</option> 
                  <option>Policy Coverage</option> 
                  <option>Vehicle Coverage</option> 
                  <option>Excluded Driver</option> 
                </select>
              </div>
            </div>
            <div class="col-sm-2">
            </div>
            <div class="col-sm-1" >
            <form onSubmit={this.handleSubmit}>
            <input type="submit" value="Add a New Question" class="btn btn-primary bottomright" />
                {/* <button class="btn btn-primary bottomright">Add a New Question</button> */}
                </form>
            </div>
          </div>
        </div>
        <div > 
          
                 
          
         </div>
         <div class="card border-primary mb-3">
         <div class="container">
        
          <br/>
             <div class="row">
              <div class="col-sm-3">
                <h6>Question UId</h6>
              </div>
             <div class="col-sm-5">              
              <h6>Question Label</h6>
            </div>
            <div class="col-sm-2">              
              <h6>Valid Values</h6>
            </div>
            <div class="col-sm-2">              
              <h6>Edit/Delete</h6>
            </div>
          </div>
          <hr/> 
          <div class="row">
              <div class="col-sm-3">
              garagingZipCode
              </div>
             <div class="col-sm-5">
             ZIP Code where vehicles are garaged / parked
            </div>
            <div class="col-sm-2">              
           
            </div>
            <div class="col-sm-2">
              <div class="btn-toolbar">
                <div class="btn-group">
                  <button type="button" class="btn btn-primary">Edit</button>
                  <button type="button" class="btn btn-secondary" >Delete</button>
                </div>
              </div>
            </div>
          </div>
          <hr/> 
          <div class="row">
              <div class="col-sm-3">
              excludedDriver
              </div>
             <div class="col-sm-5">
             Do you need to exclude anyone from coverage?
              
            </div>
            <div class="col-sm-2">
            Yes
            <br/>
            No    
            </div>
                <div class="col-sm-2">
                  <div class="btn-toolbar">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary">Edit</button>
                      <button type="button" class="btn btn-secondary" >Delete</button>
                    </div>
                  </div>
                </div>
          </div> 
          <hr/>
          <div class="row">
              <div class="col-sm-3">
              continuousLiabilityInsuranceLapseInDays
              </div>
             <div class="col-sm-5">
             Have you had liability insurance coverage in the last 5 years
            </div>
            <div class="col-sm-2">              
            Yes
            <br/>
            No
            </div>
            <div class="col-sm-2">
              <div class="btn-toolbar">
                <div class="btn-group">
                  <button type="button" class="btn btn-primary">Edit</button>
                  <button type="button" class="btn btn-secondary" >Delete</button>
                </div>
              </div>
            </div>
          </div> 
          <hr/>
       </div>
            <div>
         
        </div>
          <br/>
          <div class="row">
            <div class="col-sm-9">
            </div>
            <div class="col-sm-3">
              <ul class="pagination pull-right">
                <li class="page-item disabled">
                  <a class="page-link" href="#">&laquo;</a>
                </li>
                <li class="page-item active">
                  <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">3</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">4</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">5</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">&raquo;</a>
                </li>
              </ul>
            </div>
          {/* <button class="btn btn-primary">Add a New Question</button> */}
          {/* <button class="btn btn-secondary">Close</button> */}
          <hr/>
          </div>
        </div>  
        </div>
      </div>
      <div class="card border-primary mb-3">
        <div class="card-header">Audit History</div>
        <div class="card-body">
        <div class="container">
        <div class="row">
            <div class="col-sm-5">
            <h6>Last Updated By</h6>
            </div>
            <div class="col-sm-5">
            <h6>Last Updated On</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
           Sagar, Nikita
            </div>
            <div class="col-sm-5">
            Mon, 09 Jul 2018 12:23:29 
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-sm-5">
           Sellamuthu, Barani
            </div>
            <div class="col-sm-5">
            Mon, 09 Jul 2018 12:23:29 
            </div>
        </div>
        </div>
        </div>
      </div>
      </div>
    );
    }
  }

  export default QuestionsComponent;