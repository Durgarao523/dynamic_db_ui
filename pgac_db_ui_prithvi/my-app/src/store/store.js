import {createStore} from 'redux';
import reducer from '../reducer/reducer';
import {applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger'
/* import onDecrement from '../Action'; */
var store = applyMiddleware(thunk,logger)(createStore)(reducer);
console.log("Store values");
console.log(store.getState());
export default store;