import React from 'react';

class NewQuestionComponent extends React.Component {
    render() {
      return (
        <div>
          <div class="card border-primary mb-3 col-lg-5" style={{'max-width': '50rem;'}}>
          <div class="card-header">New Question</div>
          <div class="card-body">
            <form>
              <fieldset>
                <div class="container">
                  <div class="row">
                    <div class="col-sm-10">
                      <div class="form-group">
                        <label for="exampleInputCategory" class="text-info">Page Uid</label>
                            <select class="custom-select">
                              <option selected="">Select page</option>                  
                              <option>General Information</option>                  
                              <option>Policy Holder</option>                  
                              <option>Driver</option> 
                              <option>Vehicle Information</option> 
                              <option>Policy Coverage</option> 
                              <option>Vehicle Coverage</option> 
                              <option>Excluded Driver</option> 
                            </select>
                          </div>
                          </div>
                          </div>
                          <div class="row">
                      <div class="col-sm-10">   
                <div class="form-group">
                  <label for="inputQuestionUID">Question UID</label>
                  <input type="text" class="form-control" id="inputQuestionUID"  placeholder="Enter QuestionUID"/>
                  </div>
                </div>
                </div>
                <div class="row">
                <div class="col-sm-10">
                <div class="form-group">
                  <label for="inputQuestion">Question Label</label>
                  <input type="text" class="form-control" id="inputQuestion"  placeholder="Enter Question"/>
                  </div>
                </div>
                </div>
            <div class="row">
            <div class="col-sm-10">
                <div class="form-group">
                  <label for="inputOptions">Valid Values</label>
                
                  <textarea class="form-control" id ="inputValues" rows="3" cols="50" placeholder="Comma seperated valid values"/>
                </div>
                </div>
            </div>
            <div class="row">
            <div class="col-sm-5">
                <input type="submit" value="Submit" class="btn btn-primary btn-lg btn-block" />
            </div>
            </div>

                </div>

              </fieldset>
            </form>
        </div>
        </div>
        </div>
         
      );
    }
  }

  export default NewQuestionComponent;