const initialState={
    isLoggedIn: localStorage.getItem('isLoggedIn')
}

const inReducer =(state=initialState,action)=>{
    
    switch (action.type) {
      case "LOGIN":
               
        return {...state,isLoggedIn:action.payload};

  
      default:
        return state;
    }
  }
  export default inReducer;