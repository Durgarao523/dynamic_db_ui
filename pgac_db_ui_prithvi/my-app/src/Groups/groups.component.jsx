import React from 'react';
import Modal from 'react-modal';


const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    padding:'0px' 
  }
};


Modal.setAppElement('#app')
class GroupsComponent extends React.Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false,
      name : 'Group'
    };

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal(e) {
    this.setState({modalIsOpen: true, name :e.target.id});
  }

  afterOpenModal() {
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }
    render() {
        return(
            <div>
        <div class="card border-primary mb-3">
        <div class="card-header">Groups List</div>
        <div class="card-body">
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
            <button class="btn btn-primary bottomright" onClick={this.openModal} id='Question'>Add Question to Group</button>
           
             
            </div>
            <div class="col-sm-4">
            <button class="btn btn-primary bottomright" onClick={this.openModal} id='Page'>Add Page to Group</button>
            {/* <input type="submit" value="Add Page to Group" class="btn btn-primary bottomright" /> */}
           
            </div>
            <div class="col-sm-1" >
                <button class="btn btn-primary bottomright" onClick={this.openModal} id='Group'>Add a New Group</button>
          {/********************* Popup model Start ***************************************/}       
                <Modal
                  isOpen={this.state.modalIsOpen}
                  onAfterOpen={this.afterOpenModal}
                  onRequestClose={this.closeModal}
                  style={customStyles}
                  contentLabel="New Group Modal"
                >

          <h2 style={{background:"#3aa8e8",color:"#FFFFFF",fontSize:"20px",padding: "9px"}}>
          <span>Add {this.state.name}</span>
          <i style={{float:"right",cursor:"pointer",marginTop:"2px"}} onClick={this.closeModal} className="far fa-times-circle"></i>
          
          </h2>
          
          <div class="card-body">
            <form>
              <fieldset>
                <div class="container">
                  <div class="row">
                      <div class="col-sm-10">   
                <div class="form-group">
                  <label for="inputPageUID">{this.state.name} UID</label>
                  <input type="text" class="form-control" id="inputUID"  placeholder="Enter Id"/>
                  </div>
                </div>
                </div>
                <div class="row">
                
                <div class="col-sm-10">
                <div class="form-group">
                  <label for="inputPage">{this.state.name} Description</label>
                  <input type="text" class="form-control" id="inputPage"  placeholder="Enter  Label"/>
                  </div>
                </div>
                </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputApplication">Application</label>
                  <input type="text" class="form-control" id="inputApplication"  placeholder="Enter Application"/>
                </div>
                </div>
                <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputState">State</label>
                  <input type="text" class="form-control" id="inputState"  placeholder="Enter State"/>
                </div>
                </div>
            </div>
            <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="exampleInputCategory" className="text-info">Group
                                            Uid</label>
                                        <select className="custom-select">
                                            <option selected="">Select page</option>
                                            <option>General Information</option>
                                            <option>Policy Holder</option>
                                            <option>Driver</option>
                                            <option>Vehicle Information</option>
                                            <option>Policy Coverage</option>
                                            <option>Vehicle Coverage</option>
                                            <option>Excluded Driver</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
            <div class="row">
            <div class="col-sm-5">
                <input type="submit" value="Submit" class="btn btn-primary btn-lg btn-block" />
            </div>
            </div>

                </div>

              </fieldset>
            </form>
           
            </div>
        </Modal>
        {/********************* Popup model end ***************************************/} 
            </div>
          </div>
          <br/>
        </div>
        <div > 
          
                 
          
         </div>
         <div class="card border-primary mb-3">
         <div class="container">
        
          <br/>
             <div class="row">
              <div class="col-sm-3">
                <h6>Groups UId</h6>
              </div>
             <div class="col-sm-5">              
              <h6>Groups Label</h6>
            </div>
            <div class="col-sm-2">              
              <h6>Application</h6>
            </div>
            <div class="col-sm-2">              
              <h6>Edit/Delete</h6>
            </div>
          </div>
          <hr/> 
          <div class="row">
              <div class="col-sm-3">
              generalInformation
              </div>
             <div class="col-sm-5">
             General Info
            </div>
            <div class="col-sm-2">              
           
            </div>
            <div class="col-sm-2">
              <div class="btn-toolbar">
                <div class="btn-group">
                  <button type="button" class="btn btn-primary">Edit</button>
                  <button type="button" class="btn btn-secondary" >Delete</button>
                </div>
              </div>
            </div>
          </div>
          <hr/> 
          <div class="row">
              <div class="col-sm-3">
              policyHolder
              </div>
             <div class="col-sm-5">
             Driver 1 (Named Insured)
              
            </div>
            <div class="col-sm-2">
            
            
                
            </div>
                <div class="col-sm-2">
                  <div class="btn-toolbar">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary">Edit</button>
                      <button type="button" class="btn btn-secondary" >Delete</button>
                    </div>
                  </div>
                </div>
          </div> 
          <hr/>
          <div class="row">
              <div class="col-sm-3">
              driver
              </div>
             <div class="col-sm-5">
             Drivers
            </div>
            <div class="col-sm-2">              
            
            
            
            </div>
            <div class="col-sm-2">
              <div class="btn-toolbar">
                <div class="btn-group">
                  <button type="button" class="btn btn-primary">Edit</button>
                  <button type="button" class="btn btn-secondary" >Delete</button>
                </div>
              </div>
            </div>
          </div> 
          <hr/>
          <div class="row">
              <div class="col-sm-3">
              vehicle
              </div>
             <div class="col-sm-5">
             Vehicles
            </div>
            <div class="col-sm-2">              
            
            
            
            </div>
            <div class="col-sm-2">
              <div class="btn-toolbar">
                <div class="btn-group">
                  <button type="button" class="btn btn-primary">Edit</button>
                  <button type="button" class="btn btn-secondary" >Delete</button>
                </div>
              </div>
            </div>
          </div> 
          <hr/>
          <div class="row">
              <div class="col-sm-3">
              policyCoverage
              </div>
             <div class="col-sm-5">
             View/Change Coverage Summary
            </div>
            <div class="col-sm-2">              
            
            
            
            </div>
            <div class="col-sm-2">
              <div class="btn-toolbar">
                <div class="btn-group">
                  <button type="button" class="btn btn-primary">Edit</button>
                  <button type="button" class="btn btn-secondary" >Delete</button>
                </div>
              </div>
            </div>
          </div> 
          <hr/>
          <div class="row">
              <div class="col-sm-3">
              vehicleCoverage
              </div>
             <div class="col-sm-5">
             Vehicle Coverages
            </div>
            <div class="col-sm-2">              
            
            
            
            </div>
            <div class="col-sm-2">
              <div class="btn-toolbar">
                <div class="btn-group">
                  <button type="button" class="btn btn-primary">Edit</button>
                  <button type="button" class="btn btn-secondary" >Delete</button>
                </div>
              </div>
            </div>
          </div> 
          <hr/>
          <div class="row">
              <div class="col-sm-3">
              excludedDriver
              </div>
             <div class="col-sm-5">
             Excluded Driver
            </div>
            <div class="col-sm-2">              
           
            </div>
            <div class="col-sm-2">
              <div class="btn-toolbar">
                <div class="btn-group">
                  <button type="button" class="btn btn-primary">Edit</button>
                  <button type="button" class="btn btn-secondary" >Delete</button>
                </div>
              </div>
            </div>
          </div> 
          <hr/>
       </div>
            <div>
         
        </div>
          <br/>
          <div class="row">
            <div class="col-sm-9">
            </div>
            <div class="col-sm-3">
              <ul class="pagination pull-right">
                <li class="page-item disabled">
                  <a class="page-link" href="#">&laquo;</a>
                </li>
                <li class="page-item active">
                  <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">3</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">4</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">5</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">&raquo;</a>
                </li>
              </ul>
            </div>
          {/* <button class="btn btn-primary">Add a New Question</button> */}
          {/* <button class="btn btn-secondary">Close</button> */}
          <hr/>
          </div>
        </div>  
        </div>
      </div>
     
      </div>
      );//return
    }//render
    }//component

export default GroupsComponent;