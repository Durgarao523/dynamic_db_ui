import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './app.jsx';

import Store from './store/store';
import {Provider} from 'react-redux';
ReactDOM.render(<Provider store={Store}>
    <App/>
  </Provider>,document.getElementById('app'));
 
