import React from 'react';
import {connect} from 'react-redux';
import {loginAction} from '../actions/actions';
class LoginComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {username: '',password:''};
    
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.loginFunction = this.loginFunction.bind(this);
      }
    
      handleUsernameChange(event) {
        this.setState({username: event.target.value});
      }

      handlePasswordChange(event) {
        this.setState({password: event.target.value});
      }
    
      handleSubmit(event) {
        alert('A name was submitted: ' + this.state.username+"-"+this.state.password);
        event.preventDefault();
        
      }
      loginFunction(){
        localStorage.setItem("isLoggedIn",true);
        this.props.loginAction(true);  
        this.props.history.push('/question')
      }
      
    render(){       

        return(
            <div>
        <form onSubmit={this.handleSubmit}>
        <div class="card border-primary mb-3  col-lg-6" style={{'max-width': '50rem;'}}>
        <div class="card-header">Login</div>
        <div class="card-body">
          <h4 class="card-title"></h4>
          <div class="form-group">
              <label for="exampleInputUserName">UserName</label>              
              <input type="text" class="form-control" value={this.state.username}  onChange={this.handleUsernameChange}/>
          </div>
          <div class="form-group">
         <label for="exampleInputUserName">Password</label>         
         <input type="password" class="form-control" value={this.state.password}  onChange={this.handlePasswordChange}/>         
        </div>

          <br/>
          <div class="form-group">
                 <button type="button" class="btn btn-primary btn-lg btn-block" 
                onClick={this.loginFunction}>Login</button>
               {/*  <input type="submit" value="Submit" class="btn btn-primary btn-lg btn-block" /> */}
        </div>
        </div>
      </div>
      </form>
      </div>
    );
    }
  }
  var mapDispatchToProps = function(dispatch){
    console.log("inside map dispatch to props")
      return {
       
        loginAction:()=>{
          
          dispatch(loginAction(true))
        }
        
      }
    }
  export default connect(null,mapDispatchToProps)(LoginComponent);