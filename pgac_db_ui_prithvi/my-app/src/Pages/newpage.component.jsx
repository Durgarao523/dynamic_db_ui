import React from 'react';

class NewPageComponent extends React.Component {
    render() {
      return (
        <div>
          <div class="card border-primary mb-3 col-lg-8" style={{'max-width': '50rem;'}}>
          <div class="card-header">New Page</div>
          <div class="card-body">
            <form>
              <fieldset>
                <div class="container">
                  <div class="row">
                      <div class="col-sm-10">   
                <div class="form-group">
                  <label for="inputPageUID">Page UID</label>
                  <input type="text" class="form-control" id="inputPageUID"  placeholder="Enter PageUID"/>
                  </div>
                </div>
                </div>
                <div class="row">
                
                <div class="col-sm-10">
                <div class="form-group">
                  <label for="inputPage">Page Description</label>
                  <input type="text" class="form-control" id="inputPage"  placeholder="Enter Page"/>
                  </div>
                </div>
                </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputApplication">Application</label>
                  <input type="text" class="form-control" id="inputApplication"  placeholder="Enter Page"/>
                </div>
                </div>
                <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputState">State</label>
                  <input type="text" class="form-control" id="inputState"  placeholder="Enter Page"/>
                </div>
                </div>
            </div>
            <div class="row">
            <div class="col-sm-5">
                <input type="submit" value="Submit" class="btn btn-primary btn-lg btn-block" />
            </div>
            </div>

                </div>

              </fieldset>
            </form>
        </div>
        </div>
        </div>
         
      );
    }
  }

  export default NewPageComponent;