import React from 'react';

class PagesComponent extends React.Component {
    render() {
      return (
        <div>
        <div class="card border-primary mb-3">
        <div class="card-header">Pages List</div>
        <div class="card-body">
        <div class="container">
          <div class="row">
            <div class="col-sm-9">
            </div>
            <div class="col-sm-2" >
            <form onSubmit={this.handleSubmit}>
            <input type="submit" value="Add a New Page" class="btn btn-primary bottomright" />

                </form>
                <br/>
            </div>
          </div>
        </div>
        <div > 
         </div>
         <div class="card border-primary mb-3">
         <div class="container">
        
          <br/>
             <div class="row">
              <div class="col-sm-2">
                <h6>Page UId</h6>
              </div>
             <div class="col-sm-2">              
              <h6>Application</h6>
            </div>
            <div class="col-sm-1">              
              <h6>State</h6>
            </div>
            <div class="col-sm-2">              
              <h6>Page Label</h6>
            </div>
            <div class="col-sm-3">              
              <h6>Description</h6>
            </div>
            <div class="col-sm-2">              
              <h6>Edit/Delete</h6>
            </div>
          </div>
          <hr/> 
          <div class="row">
              <div class="col-sm-2">
              generalInformation
              </div>
             <div class="col-sm-2">
             TGR
            </div>
            <div class="col-sm-1">              
              
            </div>
            <div class="col-sm-2">              
            General Information
            </div>
            <div class="col-sm-3">              
            General information about the primary driver for the policy.
            </div>
            <div class="col-sm-2">
              <div class="btn-toolbar">
                <div class="btn-group">
                  <button type="button" class="btn btn-primary">Edit</button>
                  <button type="button" class="btn btn-secondary" >Delete</button>
                </div>
              </div>
            </div>
          </div>
          <hr/> 
          <div class="row">
              <div class="col-sm-2">
              driver
              </div>
             <div class="col-sm-2">
              TGR
            </div>
            <div class="col-sm-1">  
            </div>
            <div class="col-sm-2">
            Driver
              </div>
            <div class="col-sm-3">
            Driver Information
            </div>
                <div class="col-sm-2">
                  <div class="btn-toolbar">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary">Edit</button>
                      <button type="button" class="btn btn-secondary" >Delete</button>
                    </div>
                  </div>
                </div>
          </div> 
          <hr/>
          <div class="row">
              <div class="col-sm-2">
              vehicle
              </div>
             <div class="col-sm-2">
            TGR
            </div>
            <div class="col-sm-1">              
              
            </div>
            <div class="col-sm-2">              
           Vehicle Information
            </div>
            <div class="col-sm-3">              
           Vehicle Information
            </div>
            <div class="col-sm-2">
              <div class="btn-toolbar">
                <div class="btn-group">
                  <button type="button" class="btn btn-primary">Edit</button>
                  <button type="button" class="btn btn-secondary" >Delete</button>
                </div>
              </div>
            </div>
          </div> 
          <hr/>
       </div>
            <div>
         
        </div>
          <br/>
          <div class="row">
            <div class="col-sm-9">
            </div>
            <div class="col-sm-3">
              <ul class="pagination pull-right">
                <li class="page-item disabled">
                  <a class="page-link" href="#">&laquo;</a>
                </li>
                <li class="page-item active">
                  <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">3</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">4</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">5</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">&raquo;</a>
                </li>
              </ul>
            </div>
          {/* <button class="btn btn-primary">Add a New Question</button> */}
          {/* <button class="btn btn-secondary">Close</button> */}
          <hr/>
          </div>
        </div>  
        </div>
      </div>
     
      </div>
      )}
    }

export default PagesComponent;